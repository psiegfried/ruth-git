import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;

public class StartGyro {

 public static void main(String[] args) {
  
  EV3LargeRegulatedMotor motorL = new EV3LargeRegulatedMotor(MotorPort.C);
  EV3LargeRegulatedMotor motorR = new EV3LargeRegulatedMotor(MotorPort.B);
  
  float trackWidth = 11.8f;
  float wheelDiameter = 5.4f;
  DifferentialPilot diffPilot = new DifferentialPilot(trackWidth, wheelDiameter, motorL, motorR, false);
  diffPilot.setTravelSpeed(5);
  diffPilot.setAcceleration(999);
  
  Port port = SensorPort.S1;
  EV3GyroSensor gyroSensor = new EV3GyroSensor(port);
  //SimpleIR simpleIRLeft = new SimpleIR(iRSensorLeft);
  //Button.waitForAnyPress();
  SimpleIR sample = new SimpleIR(gyroSensor.getRateMode());
  //gyroSensor.getAngleMode()
  diffPilot.forward();
  float max = 0;
  boolean forward = true;
  while(true){
	  //diffPilot.travel(2);
   System.out.println(max);
   
   if(sample.getSample()>max) {
    if((sample.getSample()-max)>8)
    {
    	Sound.beep();
 	   diffPilot.stop();
 	   if(forward){
 		   diffPilot.backward();
 		   forward = false;
 	   }else{
 		   diffPilot.forward();
 		   forward = true;
 	   }
    }
    max = sample.getSample();
    
    
    
   }
   
	   
  

   
  }
  

 }

}