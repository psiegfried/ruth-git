import lejos.hardware.Sound;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.*;
import lejos.hardware.sensor.EV3TouchSensor;

public class HelloWorld {

	public static void main(String[] args) {
		
		EV3LargeRegulatedMotor motorL = new EV3LargeRegulatedMotor(MotorPort.C);
		EV3LargeRegulatedMotor motorR = new EV3LargeRegulatedMotor(MotorPort.B);
		
		float trackWidth = 11.8f;
		float wheelDiameter = 5.4f;
		DifferentialPilot diffPilot = new DifferentialPilot(trackWidth, wheelDiameter, motorL, motorR, false);
		diffPilot.setTravelSpeed(200000.0);
		diffPilot.setAcceleration(9999);
		
		Port port = SensorPort.S1;
		EV3TouchSensor touchSensor = new EV3TouchSensor(port);
		SimpleTouch touch = new SimpleTouch(touchSensor);
		
		int i = 0;
		
		while(i<5) {
			
			diffPilot.forward();

			if(touch.isPressed()) {
				diffPilot.stop();
				Sound.beep();
				i++;
				//diffPilot.backward();
				diffPilot.travel(-50);
				diffPilot.rotate(1100);
			} 
		
	
		}
		
				

		
		
		

		

	}

}
