import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.robotics.SampleProvider;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.*;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.navigation.Navigator;

public class UltraSonic {
	
	Navigator nav;

	public static void main(String[] args) {
		Brick brick = BrickFinder.getLocal();
		
		EV3LargeRegulatedMotor motorL = new EV3LargeRegulatedMotor(brick.getPort("C"));
		EV3LargeRegulatedMotor motorR = new EV3LargeRegulatedMotor(brick.getPort("B"));
		
		float trackWidth = 11.8f;
		float wheelDiameter = 5.4f;
		DifferentialPilot diffPilot = new DifferentialPilot(trackWidth, wheelDiameter, motorL, motorR, false);
		diffPilot.setTravelSpeed(20);
		diffPilot.setAcceleration(100);
		diffPilot.rotateRight();

		
		
		
		
		while(true) {
			// Get an instance of the Ultrasonic EV3 sensor©
			SensorModes sensor = new EV3UltrasonicSensor(SensorPort.S1);

			// get an instance of this sensor in measurement mode
			SampleProvider distance= sensor.getMode("Distance");

			// initialise an array of floats for fetching samples
			float[] sample = new float[distance.sampleSize()];

			if(sample[0]<0.7){
				diffPilot.stop();
				diffPilot.forward();
			}
			else{
				diffPilot.backward();
			}
			
			
		}
		
				

	}
		
		

		

	}