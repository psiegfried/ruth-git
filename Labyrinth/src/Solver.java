import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.navigation.DifferentialPilot;

import java.util.ArrayList;

public class Solver {

	private EV3LargeRegulatedMotor motorL = new EV3LargeRegulatedMotor(MotorPort.C);
	private EV3LargeRegulatedMotor motorR = new EV3LargeRegulatedMotor(MotorPort.B);
	  
	private float trackWidth = 11.8f;
	private float wheelDiameter = 5.4f;
	private DifferentialPilot diffPilot = new DifferentialPilot(trackWidth, wheelDiameter, motorL, motorR, false);
	
	private EV3IRSensor iRSensor = new EV3IRSensor(SensorPort.S2);
	private SimpleIR ir = new SimpleIR(iRSensor);
	private ArrayList<Integer> dirs = new ArrayList<Integer>();
	
	private final int ninety = 530;
	
	public Solver() {
		
		diffPilot.setTravelSpeed(20);
		diffPilot.setAcceleration(9999);
		
		solve();
		
		//getColor();
		
	

		
	}
	
	public void getColor() {
		
		while(true) {
			
			System.out.println(ir.getSample());
			
		}
		
		// white = 70 < x < 90
		
	}
	
	public int getLastDirection() {
		// 0 front, 1 right, 2 back, 3 left
		
		return 0;
	}
	
	public int getNextDirection() {
		
		int lastDir = dirs.get(dirs.size()-1);
		lastDir++;
		lastDir = lastDir%4;
		System.out.println(lastDir);
		return lastDir;
		
	}
	
	public void solve() {
		
		diffPilot.forward();
		dirs.add(99);
		dirs.add(0);
		
		while(true) {
			
			if(hit()) {
				
				Sound.beep();
				diffPilot.stop();
				diffPilot.travel(-10);
				
				crossroads(0);
				/*
				diffPilot.setAcceleration(10);
				diffPilot.rotate(ninety);
				
				
				diffPilot.setAcceleration(9999);
				dirs.add(getNextDirection());

				diffPilot.forward();	*/
			}
			
		}
		
	}
	
	public boolean hit() {
		
		if(ir.getSample()<90 && ir.getSample()>70) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
	public void crossroads(int i) {
		diffPilot.setAcceleration(10);

		if(i == 0) {
			
			diffPilot.rotate(-ninety);
			crossroads(i++);
			
		} else if(i == 1) {
			diffPilot.rotate(2*ninety);
			crossroads(i++);
			
		} else {
			diffPilot.rotate(ninety);
			
		}
		
		diffPilot.setAcceleration(9999);
		
	}
	
	
}
