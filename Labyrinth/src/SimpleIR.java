import lejos.robotics.SampleProvider;
import lejos.robotics.filter.AbstractFilter;

public class SimpleIR extends AbstractFilter {
	
	private float[] sample;
	
	public SimpleIR(SampleProvider source) {
		
		    super(source);
		    sample = new float[sampleSize];
		    
		  }
	
	public float getSample() {
	    super.fetchSample(sample, 0);
	    return sample[0];

	}
	
}
