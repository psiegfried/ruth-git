import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;


public class LightSensor {

	public static void main(String[] args) {
		
		EV3LargeRegulatedMotor motorL = new EV3LargeRegulatedMotor(MotorPort.C);
		EV3LargeRegulatedMotor motorR = new EV3LargeRegulatedMotor(MotorPort.B);
		
		float trackWidth = 11.8f;
		float wheelDiameter = 5.4f;
		DifferentialPilot diffPilot = new DifferentialPilot(trackWidth, wheelDiameter, motorL, motorR, false);
		diffPilot.setTravelSpeed(300);
		diffPilot.setAcceleration(300);
		
		Port port = SensorPort.S1;
		EV3IRSensor iRSensorLeft = new EV3IRSensor(port);
		SimpleIR simpleIRLeft = new SimpleIR(iRSensorLeft);
		

		Port port2 = SensorPort.S2;
		EV3IRSensor iRSensorRight = new EV3IRSensor(port2);
		SimpleIR simpleIRRight = new SimpleIR(iRSensorRight);

		
		boolean foundLeft = false;
		boolean foundRight = false;

		boolean activeLeft = true;
		boolean activeRight = true;
		
		int counter = 0;

		float sampleLeft;
		float sampleRight;
		
		Button.waitForAnyPress();
		
		while(true) {
			
			Delay.msDelay(150);
			sampleLeft = simpleIRLeft.getSample();
			sampleRight = simpleIRRight.getSample();
			
			System.out.println("Links " + sampleLeft);
			System.out.println("Rechts " + sampleRight);
			

			diffPilot.travel(3);
			
			//float sample = simpleIRRight.getSample();
			
			if(sampleLeft > 50.0 && sampleLeft < 95.0) {
				
				if(activeLeft){
					foundLeft = true;
					activeRight = false;
					counter = 0;
				}else{
					activeLeft = true;
					activeRight = false;
				}
				
			}
			else if(sampleRight > 50.0 && sampleRight < 95.0 ) {
				
				if(activeRight){
					foundRight = true;
					activeLeft = false;
					counter = 0;
				}else{
					activeRight = true;
					activeLeft = false;
				}
				
				
			}
			
			else if(foundLeft && sampleLeft < 40 && counter < 3 ){
				
				diffPilot.stop();
				diffPilot.travel(-3);
				diffPilot.rotate(-30);
				
				counter++;
				
			}
			
			else if(foundRight && sampleRight < 40 && counter < 3){
				
				diffPilot.stop();
				diffPilot.travel(-3);
				diffPilot.rotate(30);
				
				counter++;
				
			}
		}
		
		
	}

}
